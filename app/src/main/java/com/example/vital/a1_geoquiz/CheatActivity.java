package com.example.vital.a1_geoquiz;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheatActivity extends AppCompatActivity {

    private static final String EXTRA_ANSWER_IS_TRUE = "com.example.vital.a1_geoquiz.answer_is_true";   //Создание константы. Уточнение дополнений
    // именем пакета предотвращает конфликты имен с дополнениями других приложений
    private static final String EXTRA_ANSWER_SHOWN = "com.example.vital.a1_geoquiz.answer_answer_shown";    //Константа для передачи в родителя
    // совершения действий пользователем в дочерней активности
    private boolean mAnswerIsTrue;

    private TextView mAnswerTextView;
    private Button mShowAnswerButton;

    public static Intent newIntent (Context packageContext, boolean answerIsTrue) //Создание инкапсулированного метода для передачи данных!
    // По уму и безопасности)
    {
        Intent intent = new Intent(packageContext,CheatActivity.class);
        intent.putExtra(EXTRA_ANSWER_IS_TRUE,answerIsTrue);
        return intent;
    }

    public static boolean wasAnswerShown (Intent result) {  //Декодирование интента результата
        return result.getBooleanExtra(EXTRA_ANSWER_SHOWN,false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheat);

        mAnswerIsTrue = getIntent().getBooleanExtra(EXTRA_ANSWER_IS_TRUE,false);

        mAnswerTextView = findViewById(R.id.answer_text_view);
        mShowAnswerButton = findViewById(R.id.show_answer_button);
        mShowAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAnswerIsTrue) {
                    mAnswerTextView.setText(R.string.true_button);
                } else {
                    mAnswerTextView.setText(R.string.false_button);
                }
                setAnswerShownResult(true); //закрытый метод для передачи "совершал ли действие пользователь" родителю от дочерней
                // активности
            }

            private void setAnswerShownResult(boolean isAnswerShown) {
                Intent data = new Intent();
                data.putExtra(EXTRA_ANSWER_SHOWN, isAnswerShown);
                setResult(RESULT_OK,data);
            }
        });
    }
}
